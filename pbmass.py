#!/usr/bin/env python3

__author__ = 'Rafał Jereczek'
__copyright__ = 'Copyright (c) Rafał Jereczek'
__email__ = 'rafal28021996@gmail.com'
__version__ = '1.0'

import requests
import sys
import os
import os.path
import argparse


def parse_argv():
    """return command line arguments"""
    parser = argparse.ArgumentParser(description="Upload files to pastebin")
    parser.add_argument("input_file", type=str,
                        help="directory with files to upload")
    parser.add_argument("-f", "--file", type=str, nargs='?',
                        help="name of file to save results names of uploaded files")
    args = parser.parse_args()
    return args


def reading_text(file_name) -> str:
    """return text from files"""
    with open(file_name, "r") as f:
        data = f.read()
        print(data)
        return data


def upload(file_text, paste_name) -> str:
    """return response from Pastebin API"""

    # Add your Dev Key in api_dev_key.txt.txt file
    with open("api_dev_key.txt") as a:
        key = a.read()
        api_dev_key = key

    url = "https://pastebin.com/api/api_post.php"

    payload = {'api_dev_key.txt': api_dev_key,
               'api_option': 'paste',
               'api_paste_code': file_text,
               "api_paste_name": paste_name,
               'api_paste_expire_date': '10M',
               'api_paste_format': 'python'
               }

    r = requests.post(url, payload)
    return r.text


def main():
    """main function"""
    args = parse_argv()
    input_file = args.input_file
    save_dir = args.file
    py_files = list(filter(lambda x: x.endswith('.py'), os.listdir(input_file)))
    if len(sys.argv) - 1 == 3:
        with open(save_dir, 'w') as a:
            for i in range(len(py_files)):
                a.write(str(upload(reading_text(py_files[i]), save_dir) + os.linesep))
    elif len(sys.argv)-1 == 1:
        for j in range(len(py_files)):
            print(str(upload(reading_text(py_files[j]), save_dir) + os.linesep))


if __name__ == '__main__':
    main()
